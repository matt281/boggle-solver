#!/usr/bin/env python3

import argparse
from copy import deepcopy

def search(x,y,word,xsize,ysize,history):
    if len(word) == length:
        return word

    if x < 0 or y < 0:
        return "."
    if x > xsize -1 or y > ysize - 1:
        return "."

    if history[y][x] == 1:
        return "."

    history[y][x] = 1
    hop = [
        [1,0],
        [1,1],
        [0,1],
        [-1,1],
        [-1,0],
        [-1,-1],
        [0,-1],
        [1,-1],
    ]
    for i in range(len(hop)):
        result = search(x+hop[i][0],y+hop[i][1],word + letters[y][x],xsize,ysize,deepcopy(history))
        if len(result) == length:
            words[result] = result
            return "."

    return "."


parser = argparse.ArgumentParser(description='Solve boggle puzzle')

parser.add_argument('--word-length', dest='word_length', action='store',
                    type=int, default=5,
                    help='Word length')

parser.add_argument('--all', dest='all', action='store_true',
                    default=False,
                    help='Find also words not in dictionary')

args = parser.parse_args()

length = args.word_length
All = args.all

# Read from file into matrix
f = open("puzzle.txt", "r")
letters = []
for line in f:
    letters.append(list(line.rstrip()))
f.close()

# Zero out history
history = []
ysize = len(letters)
for y in range(ysize):
    xsize = len(letters[y])
    history.append([])
    for x in range(xsize):
        history[y].append(0)

# Iterate over coordinates and collect words
words = {}
for y in range(ysize):
    xsize = len(letters[y])
    for x in range(xsize):
        search(x,y,"",xsize,ysize,deepcopy(history))

if All:
    for w in words.keys():
        print(w)
else:
    # Read disctionary
    dicwords = {}
    f = open("/usr/share/dict/british-english", "r")
    for line in f:
        dicwords[line.rstrip()] = line.rstrip()
    f.close()
    f = open("/usr/share/dict/american-english", "r")
    for line in f:
        dicwords[line.rstrip()] = line.rstrip()
    f.close()

    for w in words.keys():
        if w in dicwords.keys():
            print(w)
